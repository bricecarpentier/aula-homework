#! /usr/local/bin/node
/* eslint-disable no-console,global-require,import/no-dynamic-require */

const bluebird = require('bluebird');
const pkg = require('../package.json');
const program = require('commander');

global.Promise = bluebird;

program
  .version(pkg.version)
  .command('service <serviceName>')
  .action((serviceName) => {
    try {
      const ServiceClass = require(`../services/${serviceName}`);
      const service = new ServiceClass();
      service.run();
    } catch (error) {
      console.error(`service ${serviceName} could not be found`, error);
    }
  });

program.parse(process.argv);
