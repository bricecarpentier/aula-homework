FROM node:alpine

RUN mkdir /code
RUN mkdir -p /node_modules/.bin
WORKDIR /code

ENV PATH /code/node_modules/.bin:/node_modules/.bin:§$PATH:/code/bin/

COPY package.json /code
COPY yarn.lock /code

# packages
# RUN mkdir -p /code/packages/NAME
# COPY ./packages/NAME/package.json /code/packages/NAME/package.json
RUN mkdir -p /code/packages/es
COPY ./packages/es/package.json /code/packages/es/package.json
RUN mkdir -p /code/packages/service
COPY ./packages/service/package.json /code/packages/service/package.json

# services
# RUN mkdir -p /code/services/NAME
# COPY ./services/NAME/package.json /code/services/NAME/package.json
RUN mkdir -p /code/services/api
COPY ./services/api/package.json /code/services/api/package.json
RUN mkdir -p /code/services/files
COPY ./services/files/package.json /code/services/files/package.json
RUN mkdir -p /code/services/fingerprinter
COPY ./services/fingerprinter/package.json /code/services/fingerprinter/package.json
RUN mkdir -p /code/services/metadata
COPY ./services/metadata/package.json /code/services/metadata/package.json

RUN yarn \
  --prod \
  --ignore-optional \
  --frozen-lockfile \
  --modules-folder /node_modules \
  && yarn cache clean

RUN yarn global add nodemon

COPY . /code
