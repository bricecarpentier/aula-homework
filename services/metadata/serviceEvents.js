module.exports = {
  METADATA_UPDATED: 'metadata_updated',
  GET_METADATA_FOR_URL: 'get_metadata_for_url',
};
