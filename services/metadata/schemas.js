const { Schema } = require('mongoose');

const MetadataSchema = new Schema({
  url: { type: String, index: true },
  title: { type: String },
  artist: { type: String },
  album: { type: String },
  releaseDate: { type: Date },
});

module.exports = [
  { name: 'Metadata', schema: MetadataSchema },
];
