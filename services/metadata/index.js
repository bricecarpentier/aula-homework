// @ts-check

const { constant, first, get } = require('lodash/fp');
const axios = require('axios');
const cote = require('cote');
const { DateTime } = require('luxon');

const StateSchema = require('@aulabrice/es/schemas/state');
const CoteSubscriber = require('@aulabrice/es/subscribers/cote');
const Service = require('@aulabrice/service');

const events = require('./serviceEvents');
const schemas = require('./schemas');

class MetadataService extends Service {
  constructor() {
    super();
    this.configure({
      name: 'metadata',
      dbSchemas: [
        StateSchema,
        ...schemas,
      ],
    });
    this.subscriber = null;
  }

  async start() {
    this.subscriber = new CoteSubscriber({
      publisherKey: 'es:fingerprinter',
      subscriberKey: 'metadata',
    });
    this.subscriber.on(CoteSubscriber.EVENTS.DATA, this.onData.bind(this));
    this.subscriber.start();

    this.publisher = new cote.Publisher({ name: 'metadata:publisher', key: 'metadata' });

    this.responder = new cote.Responder({ name: 'metadata:responder', key: 'metadata' });
    this.responder.on(events.GET_METADATA_FOR_URL, this.onGetMetadata.bind(this));
  }

  async stop() {
    this.subscriber.removeAllListeners();
    await this.subscriber.stop();
    this.responder.removeAllListeners();
    await this.responder.close();
    this.publisher.removeAllListeners();
    this.publisher.close();
  }

  async onData(event) {
    const Metadata = this.mongoose.model('Metadata');
    const { type, payload: { url } } = event;
    if (type === 'file_removed') {
      return Metadata.remove({ url }).exec();
    } else if (type === 'fingerprint_updated') {
      const { payload: { fingerprint, duration } } = event;
      return this.fetchMetadataForFingerprint({ fingerprint, duration })
        .then(data => this.store(url, data));
    }

    return null;
  }

  async fetchMetadataForFingerprint({ fingerprint, duration }) {
    const params = {
      client: process.env.ACOUSTID_KEY,
      duration,
      meta: 'recordings',
      fingerprint,
    };
    // @ts-ignore
    return axios.get(`${process.env.ACOUSTID_URL}lookup?meta=recordings+releases`, { params })
      .then(get('data'))
      .catch(constant({}));
  }

  async store(url, data) {
    const { status, results } = data;
    if (status !== 'ok') {
      return null;
    }

    const result = first(results) || {};
    const { score } = result;
    if (score < 0.90) {
      return null;
    }

    const { recordings } = result;
    const title = get('0.title', recordings);
    const firstRelease = get('0.releases.0', recordings) || {};
    const artist = get('0.artists.0.name', recordings);
    const album = firstRelease.title;
    const releaseDate = firstRelease.date;
    const Metadata = this.mongoose.model('Metadata');
    const metadata = new Metadata({
      url,
      title,
      artist,
      album,
      releaseDate: releaseDate
        ? DateTime.utc(releaseDate.year, releaseDate.month, releaseDate.day)
        : null,
    });
    await metadata.save();
    this.publisher.publish(events.METADATA_UPDATED, metadata.toObject());
    return metadata;
  }

  async onGetMetadata({ url }) {
    const Metadata = this.mongoose.model('Metadata');
    return Metadata.find({ url }).lean().exec().then(first);
  }
}

module.exports = MetadataService;
