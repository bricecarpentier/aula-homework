// @ts-check

const { getOr } = require('lodash/fp');
const micro = require('micro');
const { microGraphiql, microGraphql } = require('apollo-server-micro');
const { get, post, router } = require('microrouter');

const cors = require('./cors');
const schema = require('./schema');

/**
 * @typedef {object} ServerOptions
 * @property {string} wsURL
 * @property {boolean=} graphiql - enable graphiql, defaults to true
 */

/**
 * @param {any} service
 * @param {ServerOptions} options
 */
function server(service, options) {
  const graphqlHandler = microGraphql({ schema: schema(service) });
  const graphiqlHandler = microGraphiql({ endpointURL: '/graphql', subscriptionsEndpoint: options.wsURL });
  const routes = [
    ...[
      get('/graphql', graphqlHandler),
      post('/graphql', graphqlHandler),
    ],
    ...(getOr(true, 'graphiql', options) ? [get('/graphiql', graphiqlHandler)] : []),
    (req, res) => micro.send(res, 404, 'not found'),
  ];
  // @ts-ignore
  return micro(cors('*')(router(...routes)));
}

module.exports = server;
