/* eslint-disable global-require,import/no-dynamic-require */
const { makeExecutableSchema } = require('graphql-tools');

module.exports = service => makeExecutableSchema({
  typeDefs: require('./typeDefs'),
  resolvers: require('./resolvers')(service),
});
