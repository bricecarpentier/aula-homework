const typeDefs = `
type Metadata {
  title: String
  artist: String
  album: String
  releaseDate: String
}

type Song {
  url: String!
  duration: Int!
  metadata: Metadata
}

type Query {
  songs: [Song]
}

type Subscription {
  songAdded: Song
  songRemoved: Song
  metadataUpdated: Song
}
`;

module.exports = typeDefs;
