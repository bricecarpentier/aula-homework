const metadataEvents = require('@aulabrice/metadata/serviceEvents');

const getMetadata = service => file => Promise.props({
  ...file,
  metadata: service.metadataRequester
    .send({ type: metadataEvents.GET_METADATA_FOR_URL, url: file.url }),
});

const resolvers = service => ({
  Query: {
    songs: () => service.filesRequester.send({ type: 'list_files' })
      .map(getMetadata(service)),
  },
  Subscription: {
    metadataUpdated: {
      resolve: payload => ({ ...payload }),
      subscribe: () => service.pubsub.asyncIterator('metadataUpdated'),
    },
    songAdded: {
      subscribe: () => service.pubsub.asyncIterator('songAdded'),
    },
    songRemoved: {
      subscribe: () => service.pubsub.asyncIterator('songRemoved'),
    },
  },
});

module.exports = resolvers;
