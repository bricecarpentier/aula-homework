const { execute, subscribe } = require('graphql');
const { SubscriptionServer } = require('subscriptions-transport-ws');

const schema = require('./schema');

const server = (service, httpServer) => (new SubscriptionServer(
  { execute, subscribe, schema: schema(service) },
  { server: httpServer, path: '/subscriptions' },
));

module.exports = server;
