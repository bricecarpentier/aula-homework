const cors = require('micro-cors');

const enableCors = origin => cors({
  allowMethods: ['GET', 'POST'],
  origin,
});

module.exports = enableCors;
