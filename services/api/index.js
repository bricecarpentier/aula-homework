// @ts-check
const { promisify } = require('util');

const cote = require('cote');

const filesEvents = require('@aulabrice/files/serviceEvents');
const metadataEvents = require('@aulabrice/metadata/serviceEvents');
const Service = require('@aulabrice/service');
const server = require('./server');
const subscriptions = require('./subscription-server');
const { PubSub } = require('graphql-subscriptions');

const { first, omit } = require('lodash/fp');
const parseInt = require('lodash/fp').parseInt(10);

class API extends Service {
  constructor() {
    super();
    this.configure({
      name: 'API',
    });
    this.server = server(this, { wsURL: process.env.WS_URL });
    this.subscriptions = null;
    this.filesRequester = null;
    this.metadataRequester = null;
    this.pubsub = new PubSub();
  }

  async start() {
    this.filesRequester = new cote.Requester({ name: 'api:files:requester', key: 'files' });
    this.filesSubscriber = new cote.Subscriber({ name: 'api:files:subscriber', key: 'files' });
    this.filesSubscriber.on(filesEvents.FILES_ADDED, this.onFileAdded.bind(this));
    this.filesSubscriber.on(filesEvents.FILES_REMOVED, this.onFileRemoved.bind(this));
    this.metadataRequester = new cote.Requester({ name: 'api:metadata:requester', key: 'metadata' });
    this.metadataSubscriber = new cote.Subscriber({ name: 'api:metadata:subscriber', key: 'metadata' });
    this.metadataSubscriber.on(metadataEvents.METADATA_UPDATED, this.onMetadataUpdated.bind(this));

    const port = parseInt(process.env.PORT || 3000);
    const listen = promisify(this.server.listen.bind(this.server));
    await listen(port);
    this.log(`listening on ${this.server.address().port}`);
    this.subscriptions = subscriptions(this, this.server);
  }

  async stop() {
    const close = promisify(this.server.close.bind(this.server));
    await close();
    this.filesRequester.removeAllListeners();
    await this.filesRequester.close();
    this.filesSubscriber.removeAllListeners();
    this.filesSubscriber.close();
    this.metadataRequester.removeAllListeners();
    await this.metadataRequester.close();
    this.metadataSubscriber.removeAllListeners();
    this.metadataSubscriber.close();
    await this.subscriptions.close();

    this.log('stopped listening');
  }

  async onFileAdded(file) {
    this.pubsub.publish('songAdded', { songAdded: file });
  }

  async onFileRemoved(file) {
    this.pubsub.publish('songRemoved', { songRemoved: file });
  }

  async onMetadataUpdated(metadata) {
    // Should definitely add a specific request to avoid list + filter
    const file = await this.filesRequester.send({ type: filesEvents.LIST_FILES })
      // @ts-ignore
      .filter(f => f.url === metadata.url)
      .then(first);

    const withoutMongoData = omit(['_id', '__v']);
    const payload = {
      ...withoutMongoData(file),
      metadata: withoutMongoData(metadata),
    };
    // @ts-ignore
    this.pubsub.publish('metadataUpdated', payload);
  }
}

module.exports = API;
