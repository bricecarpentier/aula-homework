const urlFromPath = (basePath, baseURL) => filepath =>
  [baseURL, filepath.replace(basePath, '')].join('');

module.exports = {
  urlFromPath,
};
