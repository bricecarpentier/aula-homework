// @ts-check

module.exports = {
  FILE_CHANGED: 'file_changed',
  FILE_REMOVED: 'file_removed',
  FINGERPRINT_UPDATED: 'fingerprint_updated',
  FINGERPRINT_ERRORED: 'fingerprint_errored',
};
