// @ts-check
const { includes, isNull } = require('lodash/fp');

const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const fpcalc = require('fpcalc');

const Service = require('@aulabrice/service');

const eventTypes = require('./eventTypes');
const { urlFromPath } = require('./utils');

/**
 * @typedef FPCalcResponse
 * @property {string} duration
 * @property {string} fingerprint
 */

// The following is necessary because promisify makes ts-check falsely detect
// a parameter error
/* eslint-disable lodash-fp/no-extraneous-function-wrapping */
/**
 * @param {string} filepath
 * @returns {Promise.<FPCalcResponse> | Promise.<null>}
 */
function pFpCalc(filepath) {
  // @ts-ignore
  return promisify(fpcalc)(filepath);
}
/* eslint-enable lodash-fp/no-extraneous-function-wrapping */


class Fingerprinter extends Service {
  constructor() {
    super();
    this.configure({
      name: 'Fingerprinter',
      es: {
        version: '1',
        useCote: true,
      },
    });

    this.ignored = [
      '.DS_Store',
    ];
  }

  start() {
    this.watcher = fs.watch(process.env.MEDIA_PATH, this.onWatchEvent.bind(this));
  }

  stop() {
    this.watcher.close();
  }

  async onWatchEvent(eventType, filename) {
    if (includes(filename, this.ignored)) {
      return null;
    }

    const filepath = path.join(process.env.MEDIA_PATH, filename);
    const url = urlFromPath(process.env.MEDIA_PATH, process.env.MEDIA_URL)(filepath);
    await this.appendEvent(eventTypes.FILE_CHANGED, { eventType, filepath, url });

    if (eventType === 'rename') {
      return this.onRenameEvent(filepath, url);
    } else if (eventType === 'change') {
      return this.onFileChangeEvent(filepath, url);
    }

    return null;
  }

  async onRenameEvent(filepath, url) {
    const fileExists = await promisify(fs.access)(filepath, fs.constants.F_OK)
      .catch(err => err)
      .then(isNull);
    if (!fileExists) {
      return this.appendEvent(eventTypes.FILE_REMOVED, { filepath, url });
    }
    return null;
  }

  async onFileChangeEvent(filepath, url) {
    const fpResponse = await this.fingerprint(filepath);
    if (isNull(fpResponse)) {
      return this.appendEvent(eventTypes.FINGERPRINT_ERRORED, { filepath, url });
    }
    const { duration, fingerprint } = fpResponse;
    return this.appendEvent(eventTypes.FINGERPRINT_UPDATED, { fingerprint, duration, url });
  }

  async fingerprint(filepath) {
    try {
      const response = await pFpCalc(filepath);
      this.log(`${filepath}[${response.duration}]: ${response.fingerprint}`);
      return response;
    } catch (error) {
      this.error(error);
      return null;
    }
  }
}

module.exports = Fingerprinter;
