const utils = require('./utils');

describe('urlFromPath', () => {
  const uut = utils.urlFromPath;

  it('should remove basePath from path and concatenate with baseURL', () => {
    const actual = uut('/my-base-path/', 'https://cdn.myserver.com/')('/my-base-path/file.mp3');
    expect(actual).toBe('https://cdn.myserver.com/file.mp3');
  });
});
