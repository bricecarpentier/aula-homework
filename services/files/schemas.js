const { Schema } = require('mongoose');

const FileSchema = new Schema({
  url: { type: String, index: true },
  duration: { type: Number },
});

module.exports = [
  { name: 'File', schema: FileSchema },
];
