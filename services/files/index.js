// @ts-check
const cote = require('cote');

const StateSchema = require('@aulabrice/es/schemas/state');
const CoteSubscriber = require('@aulabrice/es/subscribers/cote');
const Service = require('@aulabrice/service');

const schemas = require('./schemas');
const events = require('./serviceEvents');

class Files extends Service {
  constructor() {
    super();
    this.configure({
      name: 'Files',
      dbSchemas: [
        StateSchema,
        ...schemas,
      ],
    });
  }

  async start() {
    this.subscriber = new CoteSubscriber({
      publisherKey: 'es:fingerprinter',
      subscriberKey: 'files',
    });
    this.subscriber.on(CoteSubscriber.EVENTS.DATA, this.onData.bind(this));
    this.subscriber.start();

    this.responder = new cote.Responder({
      name: 'files:responder',
      key: 'files',
      respondsTo: [events.LIST_FILES],
    });
    this.responder.on(events.LIST_FILES, this.onListFiles.bind(this));
    this.publisher = new cote.Publisher({
      name: 'files:publisher',
      key: 'files',
      broadcasts: [events.FILES_ADDED],
    });

    // @ts-ignore
    return Promise.delay();
  }

  async stop() {
    this.subscriber.removeAllListeners();
    await this.subscriber.stop();
    this.responder.removeAllListeners();
    await this.responder.close();
    this.publisher.removeAllListeners();
    await this.publisher.close();
  }

  onData(event) {
    const File = this.mongoose.model('File');
    if (event.type === 'file_removed') {
      const { url } = event.payload;
      this.log(`Removing file with url ${url}`);
      return File.remove({ url }).exec()
        // @ts-ignore
        .tap(() => this.publisher.publish(events.FILES_REMOVED, { url }));
    } else if (event.type === 'fingerprint_updated') {
      const { url, duration } = event.payload;
      this.log(`Adding or updating file with url ${url}`);
      const query = { url };
      const update = { url, duration };
      const options = { new: true, upsert: true };
      return File.findOneAndUpdate(query, update, options).exec()
        // @ts-ignore
        .tap(file => this.publisher.publish(events.FILES_ADDED, file));
    }

    return null;
  }

  onListFiles() {
    return this.mongoose.model('File')
      .find()
      .lean()
      .exec();
  }
}

module.exports = Files;
