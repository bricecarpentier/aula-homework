const events = {
  FILES_ADDED: 'files_added',
  FILES_REMOVED: 'files_removed',
  LIST_FILES: 'list_files',
};

module.exports = events;
