Homework
========

Project developed during the interview process for Aula

# Pre-requisite

In order to mirror the production environment as closely as possible, linkybot is expected to be accessed through HTTPS and dns-calls even in dev mode. This can easily be achieved using two tools:
- [https://wiki.archlinux.org/index.php/dnsmasq](dnsmasq): custom dns proxy. You should make it resolve *.localhost to 127.0.0.1
- [https://github.com/wavyapp/traefik-service](traefik): microservices-centered reverse proxy, tailored for wavy

# Build

```
docker-compose build
```

# Launch

```
yarn && docker-compose up # what else?
```

```
cd app && yarn && yarn start
```

# Usage

Go to http://localhost:3000/
Add and remove some files in `./media/`

Have fun!
