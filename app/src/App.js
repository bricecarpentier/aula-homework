import { flow } from 'lodash/fp';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import withApollo from './hoc/withApollo';
import withStore from './hoc/withStore';

import MenuAppBar from './MenuAppBar';
import Player from './Player';
import Songs from './Songs';

const styles = {
  root: {
    flexGrow: 1,
  },
};

class App extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Player />
        <MenuAppBar />
        <Songs />
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

const wrap = flow(
  withStyles(styles),
  withApollo({
    uri: 'https://api.homework.localhost/graphql',
    wsUri: 'wss://api.homework.localhost/subscriptions',
  }),
  withStore,
);

export default wrap(App);
