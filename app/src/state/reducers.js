import { combineReducers } from 'redux'
import actions from './actions';

function currentlyPlaying(state = null, action) {
  switch(action.type) {
    case actions.SET_CURRENTLY_PLAYING:
      console.log(action);
      return { url: action.url };
    default:
      return state;
  }
}

export default combineReducers({
  currentlyPlaying,
});
