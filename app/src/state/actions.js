const SET_CURRENTLY_PLAYING = 'set_currently_playing';

const setCurrentlyPlaying = url => ({ type: SET_CURRENTLY_PLAYING, url });

export default {
  SET_CURRENTLY_PLAYING,
  setCurrentlyPlaying,
};
