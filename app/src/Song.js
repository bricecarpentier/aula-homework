import {
  flow,
  get,
  getOr,
  first,
  last,
  split,
 } from 'lodash/fp';
import React from 'react';
import {
  IconButton,
  TableRow,
  TableCell,
} from 'material-ui';
import {
  PlayArrow,
  Pause,
} from 'material-ui-icons';
import { DateTime } from 'luxon';

const getName = flow(
  get('url'),
  split('/'),
  last,
  split('.'),
  first,
);

const getTitle = song => get('metadata.title', song) || getName(song);
const getAlbum = getOr('', 'metadata.album');

const getPlayPauseIcon = (currentPlayingURL, currentURL) =>
  currentPlayingURL === currentURL ? <Pause /> : <PlayArrow />;

const getFormattedDate = (song) => {
  const releaseDate = get('metadata.releaseDate', song);
  const d = (DateTime.fromISO(releaseDate)).toLocaleString();
  return releaseDate ? d : '';
};


const Song = (component, props) => song => (
  <TableRow key={song.url}>
    <TableCell><IconButton onClick={() => component.onPlayPause(song.url)}>{getPlayPauseIcon(props.currentlyPlaying, song.url)}</IconButton></TableCell>
    <TableCell>{getTitle(song)}</TableCell>
    <TableCell>{getAlbum(song)}</TableCell>
    <TableCell>{getFormattedDate(song)}</TableCell>
    <TableCell numeric>{song.duration}</TableCell>
  </TableRow>
);

export default Song;
