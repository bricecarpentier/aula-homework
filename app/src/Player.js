import { noop } from 'lodash/fp';
import React from 'react';
import PropTypes from 'prop-types';

import withRedux from './hoc/withRedux';


class Player extends React.Component {
  constructor() {
    super();
    this.audio = null;
  }

  setAudio = elem => this.audio = elem;

  render() {
    const url = this.props.currentlyPlaying;
    if (this.audio) {
      Promise.resolve(this.audio[url ? 'play' : 'pause']()).catch(noop);
    }

    return <audio autoPlay ref={this.setAudio} src={url}></audio>;
  }
}

Player.propTypes = {
  currentlyPlaying: PropTypes.string,
  setCurrentlyPlaying: PropTypes.func.isRequired,
};

export default withRedux(Player);
