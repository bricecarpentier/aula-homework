import React from 'react';
import { get, getOr } from 'lodash/fp';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getMainDefinition } from 'apollo-utilities';


const getClient = (options) => {
  const httpLink = new HttpLink({ uri: getOr('/graphql', 'uri', options) });
  const wsLink = new WebSocketLink({ uri: get('wsUri', options) });
  const link = split(
    // split based on operation type
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink,
  );

  return new ApolloClient({
    // By default, this client will send queries to the
    //  `/graphql` endpoint on the same host
    // Pass the configuration option { uri: YOUR_GRAPHQL_API_URL } to the `HttpLink` to connect
    // to a different host
    link,
    cache: new InMemoryCache(),
  });
};

export default options => Wrapped => class WithApollo extends React.Component {
  render() {
    return (
      <ApolloProvider client={getClient(options)}>
        <Wrapped />
      </ApolloProvider>
    );
  }
}
