import React from 'react'
import { Provider } from 'react-redux'

import store from '../state/store';

const withStore = Wrapped => class WithStore extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Wrapped />
      </Provider>
    );
  }
}

export default withStore;