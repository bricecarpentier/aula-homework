import { get } from 'lodash/fp';
import { connect } from 'react-redux';

import actions from '../state/actions';

export const mapDispatchToProps = dispatch => ({
  setCurrentlyPlaying: url => dispatch(actions.setCurrentlyPlaying(url))
});

export const mapStateToProps = state => ({
  currentlyPlaying: get('url', state.currentlyPlaying),
});


export default Wrapped => connect(mapStateToProps, mapDispatchToProps)(Wrapped);
