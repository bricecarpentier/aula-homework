import {
  compact,
  flow,
  get,
  uniqBy,
 } from 'lodash/fp';

import React, { Component } from "react";
import PropTypes from 'prop-types';
import {
  Paper,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  withStyles,
} from "material-ui";

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import withRedux from './hoc/withRedux';
import Song from './Song';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

class Songs extends Component {
  onPlayPause = url => {
    const currentlyPlaying = this.props.currentlyPlaying;
    this.props.setCurrentlyPlaying(url === currentlyPlaying ? null : url);
  }

  componentWillMount() {
    this.props.songAdded();
    this.props.songRemoved();
    this.props.metadataUpdated();
  }
  
  render() {
    const {
      classes,
      data: { songs = [] },
     } = this.props;

    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Actions</TableCell>
              <TableCell>Title</TableCell>
              <TableCell>Album</TableCell>
              <TableCell>Release date</TableCell>
              <TableCell numeric>Duration</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {songs.map(Song(this, this.props))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

Songs.propTypes = {
  classes: PropTypes.object.isRequired,
};

const query = gql`
  query AllSongs {
    songs {
      duration
      url
      metadata {
        title
        album
        releaseDate
      }
    }
  }
`;

const songAdded = gql`
  subscription {
    songAdded {
      url
      duration
    }
  }
`;
const songRemoved = gql`
  subscription {
    songRemoved {
      url
    }
  }
`;

const metadataUpdated = gql`
  subscription {
    metadataUpdated {
      url
      duration
      metadata {
        title
        artist
        album
        releaseDate
      }
    }
  }
`

const addSong = (newSong, existingSongs) =>
  uniqBy('url', compact([newSong, ...existingSongs]))

const wrap = flow(
  withRedux,
  withStyles(styles),
  graphql(query, {
    name: 'data',
    props: props => console.log(props) || ({
      ...props,
      metadataUpdated: () => props.data.subscribeToMore({
        document: metadataUpdated,
        updateQuery: (previous, current) => {
          const updatedSong = get('subscriptionData.data.metadataUpdated', current);
          return {
            ...previous,
            songs: addSong(updatedSong, previous.songs),
          };
        },
      }),
      songAdded: () => props.data.subscribeToMore({
        document: songAdded,
        updateQuery: (previous, current) => {
          const addedSong = {
            ...get('subscriptionData.data.songAdded', current),
            metadata: {
              artist: '',
              album: '',
              title: '',
              releaseDate: undefined,
            },
          };
          return {
            ...previous,
            songs: addSong(addedSong, previous.songs),
          };
        }
      }),
      songRemoved: () => props.data.subscribeToMore({
        document: songRemoved, 
        updateQuery: (previous, current) => {
          const removedURL = get('subscriptionData.data.songRemoved.url', current);
          return {
            ...previous,
            songs: previous.songs.filter(song => song.url !== removedURL),
          };
        },
      }),
    }),
  }),
  
);

export default wrap(Songs);
