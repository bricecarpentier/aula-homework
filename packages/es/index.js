/* eslint-disable */

module.exports = {
  gateways: require('./gateways'),
  schema: require('./schemas'),
  subscribers: require('./subscribers'),
};
