// @ts-check

const { get } = require('lodash/fp');
const { Schema } = require('mongoose');

const CounterSchema = new Schema({
  name: { type: String, index: true },
  count: Number,
});

CounterSchema.statics.increment = function doIncrement(name) {
  const query = { name };
  const update = { $inc: { count: 1 } };
  const options = { upsert: true, new: true };
  return this.findOneAndUpdate(query, update, options).then(get('count'));
};

module.exports = {
  name: 'Counter',
  schema: CounterSchema,
};
