// @ts-check
const { Schema } = require('mongoose');

const StateSchema = new Schema({
  name: { type: String, index: true },
  current: { type: Number, default: 0 },
});

module.exports = {
  name: 'State',
  schema: StateSchema,
};
