// @ts-check

const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const EventSchema = new Schema({
  domain: { type: String, index: true },
  version: { type: String },
  type: String,
  domainOrder: { type: Number, index: true },
  order: { type: Number, index: true },
  payload: Schema.Types.Mixed,
});

EventSchema.index({ domain: 1, domainOrder: 1 });

/**
 * @param {string} domain
 * @param {string} type
 * @param {Object} payload
 */
EventSchema.statics.createEvent = async function doCreateEvent(domain, version, type, payload) {
  const Counter = mongoose.model('Counter');
  if (!Counter) {
    throw new Error('Counter model is not defined');
  }
  const [order, domainOrder] = await Promise.all([
    // @ts-ignore
    Counter.increment('events'),
    // @ts-ignore
    Counter.increment(`events:${domain}`),
  ]);
  const instance = new this({
    type,
    domain,
    payload,
    order,
    version,
    domainOrder,
  });
  return instance.save();
};

module.exports = {
  name: 'Event',
  schema: EventSchema,
};

