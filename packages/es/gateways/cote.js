// @ts-check

const cote = require('cote');

/**
 * @typedef {function(number, number): Promise.<Array.<any>>} FetchHandler
 */

/**
 * @typedef {Object} CoteGatewayOptions
 * @property {string} key
 * @property {FetchHandler} fetchHandler
 */

const EVENTS = {
  APPEND: 'append',
  FETCH: 'fetch',
};

class CoteGateway {
  /**
   * @param {CoteGatewayOptions} options
   */
  constructor(options) {
    this.options = options;
    this.key = options.key;
    this.fetchHandler = options.fetchHandler;
  }

  async start() {
    this.publisher = await this.createPublisher();
    this.responder = await this.createResponder();
    // @ts-ignore
    return Promise.delay();
  }

  async createPublisher() {
    const publisher = new cote.Publisher({
      name: 'es:publisher',
      key: this.key,
      broadcasts: [EVENTS.APPEND],
    });
    // @ts-ignore
    await Promise.delay();
    return publisher;
  }

  async createResponder() {
    const responder = new cote.Responder({
      name: 'es:responder',
      key: this.key,
      respondsTo: [EVENTS.FETCH],
    });
    responder.on(EVENTS.FETCH, this.onFetch.bind(this));
    // @ts-ignore
    await Promise.delay();
    return responder;
  }

  appendEvent(event) {
    return this.publisher.publish(EVENTS.APPEND, event);
  }

  onFetch({ after, limit }) {
    return this.fetchHandler(after, limit);
  }

  async stop() {
    this.publisher.removeAllListeners();
    this.publisher.close();
    this.responder.removeAllListeners();
    this.responder.close();
    // @ts-ignore
    return Promise.delay();
  }
}
CoteGateway.EVENTS = EVENTS;

module.exports = CoteGateway;
