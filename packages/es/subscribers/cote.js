// @ts-check

const { EventEmitter } = require('events');
const cote = require('cote');
const mongoose = require('mongoose');

const CoteGateway = require('../gateways/cote');
const schemas = require('../schemas');

/**
 * @typedef {Object} CoteSubscriberOptions
 * @property {string} publisherKey
 * @property {string} subscriberKey
 */

const EVENTS = {
  DATA: 'data',
};

class CoteSubscriber extends EventEmitter {
  /**
   * @param {CoteSubscriberOptions} options
   */
  constructor(options) {
    super();
    this.options = options;
  }

  async start() {
    this.subscriber = await this.createSubscriber();
    this.requester = await this.createRequester();
    // @ts-ignore
    return Promise.delay();
  }

  async stop() {
    this.subscriber.removeAllListeners();
    this.subscriber.close();
    this.requester.removeAllListeners();
    this.requester.close();
  }

  async onAppend(event) {
    /* Beware: Race condition */
    const state = await this.getCurrentState();
    const { domainOrder } = event;
    const { current } = state;
    if (domainOrder === current + 1) {
      // the one we expect, let's emit it
      this.emit(EVENTS.DATA, event);
      state.current += 1;
      return state.save();
    } else if (domainOrder > current + 1) {
      // we are running behind => let's fetch
      return this.fetchMissing(current, domainOrder);
    }

    // we've already received this event, let's ignore it
    return null;
  }

  async createSubscriber() {
    const subscriber = new cote.Subscriber({
      name: 'es:subscriber',
      key: this.options.publisherKey,
      subscribesTo: [CoteGateway.EVENTS.APPEND],
    });
    subscriber.on(CoteGateway.EVENTS.APPEND, this.onAppend.bind(this));
    // @ts-ignore
    await Promise.delay();
    return subscriber;
  }

  async createRequester() {
    const requester = new cote.Requester({
      name: 'es:requester',
      key: this.options.publisherKey,
      requests: [CoteGateway.EVENTS.FETCH],
    });
    // @ts-ignore
    await Promise.delay();
    return requester;
  }

  getCurrentState() {
    const State = mongoose.model(schemas.state.name);
    const query = { name: `${this.options.subscriberKey}:${this.options.publisherKey}:es:requester` };
    const update = { ...query };
    const options = { new: true, upsert: true };
    return State.findOneAndUpdate(query, update, options).exec();
  }

  fetchMissing(currentStored, lastReceived) {
    const request = {
      type: CoteGateway.EVENTS.FETCH,
      after: currentStored,
      limit: lastReceived - currentStored,
    };
    return this.requester
      .send(request)
      // @ts-ignore
      .mapSeries(this.onAppend.bind(this));
  }
}
CoteSubscriber.EVENTS = EVENTS;

module.exports = CoteSubscriber;
