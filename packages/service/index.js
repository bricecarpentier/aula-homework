// @ts-check
const { lowerCase, uniq, values } = require('lodash/fp');

const mongoose = require('mongoose');
const esSchemas = require('@aulabrice/es/schemas');
const esGateways = require('@aulabrice/es/gateways');

/**
 * @typedef {Object} EventSourcingOptions
 * @property {string} version - event version to use
 * @property {string=} domain - event domain to use, defaults to lowerCase(name)
 * @property {boolean=} useCote - true to start cote gateway
 */

/**
 * @typedef {Object} ServiceOptions
 * @property {string} name - the service name
 * @property {Array<any>=} dbSchemas - the schemas this service is going to use
 * @property {EventSourcingOptions=} es - true to activate event-sourcing capabilities
 */

class Service {
  /**
   * @param {ServiceOptions} options
   */
  configure(options) {
    this.name = options.name || 'noname';
    this.dbSchemas = options.dbSchemas || [];
    this.mongoose = null;

    this.es = options.es;
    if (this.es) {
      this.dbSchemas = uniq([...this.dbSchemas, ...values(esSchemas)]);
    }
  }

  async doStart() {
    this.log(`>> starting ${this.name}`);
    if (this.es && this.es.useCote) {
      this.coteGateway = await this.createESCoteGateway();
      await this.coteGateway.start();
    }
    this.start();
  }

  start() {}

  async doStop() {
    this.log(`>> stoping ${this.name}`);
    if (this.coteGateway) {
      await this.coteGateway.stop();
    }
    await this.stop();
    if (this.dbSchemas.length) {
      await this.disconnectFromDatabase();
    }
  }

  stop() {}

  async run() {
    process.once('SIGUSR2', this.onSIGUSR2.bind(this));
    if (this.dbSchemas.length) {
      this.mongoose = await this.connectToDatabase(process.env.MONGODB_URL);
    }
    await this.doStart();
  }

  /*
   * Database
   */
  async initDatabase(url) {
    await this.connectToDatabase(url);
  }

  /**
   * @param {string} url
   */
  async connectToDatabase(url) {
    const connection = await mongoose.connect(url);

    const registerSchema = c => description => c.model(description.name, description.schema);

    this.dbSchemas.map(registerSchema(connection));
    return connection;
  }

  disconnectFromDatabase() {
    return this.mongoose.disconnect();
  }

  /*
   * Event-sourcing
   */

  async createESCoteGateway() {
    const CoteGateway = esGateways.cote;
    const gateway = new CoteGateway({
      key: `es:${lowerCase(this.name)}`,
      fetchHandler: this.retrieveEvents.bind(this),
    });
    return gateway;
  }

  /**
   * @param {string} type
   * @param {any} payload
   */
  async appendEvent(type, payload) {
    try {
      const model = this.mongoose.model(esSchemas.event.name);
      const {
        domain = lowerCase(this.name),
        version,
      } = this.es;
      // @ts-ignore
      const event = await model.createEvent(domain, version, type, payload);
      if (this.coteGateway) {
        await this.coteGateway.appendEvent(event.toObject());
      }
      return event;
    } catch (error) {
      this.error(error);
      return null;
    }
  }

  /**
   * @param {number} after
   * @param {number} limit
   * @returns {Promise.<any>}
   */
  async retrieveEvents(after, limit) {
    const model = this.mongoose.model(esSchemas.event.name);
    const query = {
      domain: lowerCase(this.name),
      domainOrder: { $gt: after },
    };
    return model.find(query).limit(limit).lean().exec();
  }

  /*
   * Logger
   */
  log(...params) {
    // eslint-disable-next-line no-console
    return console.log(...params);
  }

  error(...params) {
    // eslint-disable-next-line no-console
    return console.error(...params);
  }

  async onSIGUSR2() {
    await this.doStop();
    process.kill(process.pid, 'SIGUSR2');
  }
}

module.exports = Service;
